### MAILLE DEVELOPPEMENT
## CV Julien Maille

> Comme la Hongrie, le monde informatique a une langue qui lui est propre. Mais il y a une différence. Si vous restez assez longtemps avec des Hongrois, vous finirez bien par comprendre de quoi ils parlent.

This project is a CV in the form of "showcase site".
A list of the languages used:
* HTML
* CSS
* JavaScript

Ce projet est un CV sous forme de "site vitrine".
Une liste des langages utilisés:
* HTML
* CSS
* JavaScript
